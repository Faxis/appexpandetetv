import 'package:expandetetv/Pages/blog_article_show.dart';
import 'package:expandetetv/Pages/principal_page.dart';
import 'package:flutter/material.dart';

Map<String, WidgetBuilder> getApplicationRoutes() {
  return <String, WidgetBuilder>{
    '/': (BuildContext context) => PrincipalPage(),
    'articulo': (BuildContext context) => BlogArticle()
  };
}