import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;

class InicioPage extends StatefulWidget {
  InicioPage({Key key}) : super(key: key);

  @override
  _InicioPageState createState() => _InicioPageState();
}

class InicioJson {
  final String title;
  final String content;
  final int id;

  InicioJson({this.title, this.id, this.content});

  factory InicioJson.fromJson(Map<String, dynamic> json) {
    return InicioJson(
      title: json['title']['rendered'],
      id: json['id'],
      content: json['content']['rendered'],
    );
  }
}

class _InicioPageState extends State<InicioPage> {
  String removerHTMLTags(String htmlText) {
    RegExp exp = RegExp(r"<[^>]*>", multiLine: true, caseSensitive: true);

    return htmlText.replaceAll(exp, '');
  }

  Future<InicioJson> fetchPost(String url) async {
    final response = await http
        .get(url);

    if (response.statusCode == 200) {
      // Si el servidor devuelve una repuesta OK, parseamos el JSON
      return InicioJson.fromJson(json.decode(response.body));
    } else {
      // Si esta respuesta no fue OK, lanza un error.
      throw Exception('Failed to load post');
    }
  }

  //static Map userMap = jsonDecode(jsonString);
  //var jsOn = InicioJson.fromJson(userMap);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        body: ListView(
          shrinkWrap: true,
          children: <Widget>[
            logoExPandeteTV(context),
            Padding(
              padding: const EdgeInsets.only(bottom:8.0),
              child: Image.asset(
                'assets/principal.png',
                height: MediaQuery.of(context).size.height * 0.25,
              ),
            ),
            cajaBienvenido(context),
            Padding(
                 padding: const EdgeInsets.all(16),
              child: Image.asset(
                'assets/noticias.png',
                height: MediaQuery.of(context).size.height * 0.22,
              ),
            ),
            cajaNoticias(context)
          ],
        ),
      ),
    );
  }

  Container cajaNoticias(BuildContext context) {
    return Container(
      //padding: EdgeInsets.all(10),
      child: ClipPath(
        clipper: MultipleRoundedCurveClipper(),
        child: Container(
          padding: EdgeInsets.all(10),
          decoration: BoxDecoration(
            color: Colors.blue[600],
          ),
          width: MediaQuery.of(context).size.width,
          child: textoFutureBuilder('http://systemkw.com/expandetetv/wp-json/wp/v2/pages/24'),
        ),
      ),
    );
  }

  ClipPath logoExPandeteTV(BuildContext context) {
    return ClipPath(
      clipper: WaveClipperOne(),
      child: Container(
        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(
          color: Colors.teal[400],
        ),
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height * 0.4,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Image.asset(
              'assets/ex-logo.png',
              height: MediaQuery.of(context).size.height * 0.25,
            ),
          ],
        ),
      ),
    );
  }

  Widget cajaBienvenido(BuildContext context) {
    return Container(
      //padding: EdgeInsets.all(10),
      child: ClipPath(
        clipper: MovieTicketClipper(),
        child: Container(
          padding: EdgeInsets.all(10),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(0),
            color: Colors.yellow[700],
          ),
          width: MediaQuery.of(context).size.width,
          child: textoFutureBuilder('http://systemkw.com/expandetetv/wp-json/wp/v2/pages/20'),
        ),
      ),
    );
  }

  FutureBuilder<InicioJson> textoFutureBuilder(String url) {
    return FutureBuilder<InicioJson>(
      future: fetchPost(url),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return Column(
            children: <Widget>[
              Text("${snapshot.data.title}",
                  style: GoogleFonts.ralewayDots(
                      fontSize: 16.0,
                      color: Colors.white,
                      fontWeight: FontWeight.bold)),
              Text(
                removerHTMLTags(snapshot.data.content),
                textAlign: TextAlign.center,
                style: GoogleFonts.workSans(height: 1.5),
              ),
            ],
          );
        } else if (snapshot.hasError) {
          return Text("${snapshot.error}");
        }

        // Por defecto, muestra un loading spinner
        return Center(child: CircularProgressIndicator());
      },
    );
  }
}
