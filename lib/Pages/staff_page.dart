import 'package:flutter/material.dart';

import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class StaffPage extends StatefulWidget {
  StaffPage({Key key}) : super(key: key);

  @override
  _StaffPageState createState() => _StaffPageState();
}

class StaffJsonMultiple {
  final List<StaffJson> listaJson;

  StaffJsonMultiple({this.listaJson});

  factory StaffJsonMultiple.fromJson(List<dynamic> parsedJson) {
    List<StaffJson> listaJson = new List<StaffJson>();
    listaJson = parsedJson.map((i) => StaffJson.fromJson(i)).toList();

    return new StaffJsonMultiple(
      listaJson: listaJson,
    );
  }
}

class StaffJson {
  final String title;
  final String content;
  /* final String content; */
  final String image;
//this.image, this.content
  StaffJson({this.title, this.content, this.image});

  factory StaffJson.fromJson(Map<String, dynamic> json) {
    return StaffJson(
      title: json['title']['rendered'],
      image: json['media_details']['sizes']['full']['source_url'],
      content: json['caption']['rendered'],
    );
  }
}

class _StaffPageState extends State<StaffPage> {
  String removerHTMLTags(String htmlText) {
    RegExp exp = RegExp(r"<[^>]*>", multiLine: true, caseSensitive: true);

    return htmlText.replaceAll(exp, '');
  }

  @override
  void initState() {
    super.initState();
  }

  Future<StaffJsonMultiple> getData() async {
    final response = await http
        .get('http://systemkw.com/expandetetv/wp-json/wp/v2/media?parent=50');
    //print(response.body.toString());
    if (response.statusCode == 200) {
      // Si el servidor devuelve una repuesta OK, parseamos el JSON
      return StaffJsonMultiple.fromJson(json.decode(response.body));
    } else {
      // Si esta respuesta no fue OK, lanza un error.
      throw Exception('Failed to load post');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        body: Column(
          //shrinkWrap: true,
          children: <Widget>[
            ClipPath(
              clipper: MultipleRoundedCurveClipper(),
              child: Container(
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(
                  color: Colors.yellow[800],
                ),
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height * 0.35,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Image.asset(
                      'assets/staff.png',
                      height: MediaQuery.of(context).size.height * 0.28,
                    ),
                  ],
                ),
              ),
            ),
            cajaBienvenido(context)
            //aqui va cajaBienvenido >>>><<<<
          ],
        ),
      ),
    );
  }

  Widget cajaBienvenido(BuildContext context) {
    return textoFutureBuilder();
  }

  FutureBuilder<StaffJsonMultiple> textoFutureBuilder() {
    return FutureBuilder<StaffJsonMultiple>(
      future: getData(),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return Expanded(
            child: ListView.builder(
                shrinkWrap: true,
                key: widget.key,
                itemCount: snapshot.data.listaJson.length,
                itemBuilder: (context, index) {
                  return Container(
                    margin: EdgeInsets.only(top: 5, bottom: 5),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Container(
                          decoration: BoxDecoration(
                              color: Colors.white,
                              border: Border.all(
                                  color: Colors.grey[100], width: 1)),
                          padding: EdgeInsets.all(2),
                          child: ClipRRect(
                            clipBehavior: Clip.antiAliasWithSaveLayer,
                            child: FadeInImage.assetNetwork(
                              placeholder: 'assets/spinner.gif',
                              image: snapshot.data.listaJson[index].image,
                              width: 80.0,
                            ),
                            borderRadius: BorderRadius.circular(2),
                          ),
                        ),
                        new Expanded(
                          flex: 1,
                          child: Container(
                            padding: EdgeInsets.all(10),
                            child: ClipPath(
                              clipper: MovieTicketBothSidesClipper(),
                              child: Container(
                                padding: EdgeInsets.all(10),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(12),
                                  color: Colors.indigo[500],
                                ),
                                width: MediaQuery.of(context).size.width,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Text(
                                        "${snapshot.data.listaJson[index].title}",
                                        style: GoogleFonts.ralewayDots(
                                            fontSize: 16.0,
                                            color: Colors.white,
                                            fontWeight: FontWeight.bold)),
                                    Text(
                                        "${removerHTMLTags(snapshot.data.listaJson[index].content)}",
                                        style: GoogleFonts.ralewayDots(
                                            fontSize: 15.0,
                                            color: Colors.white,
                                            fontWeight: FontWeight.normal)),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  );
                }),
          );
        } else if (snapshot.hasError) {
          return Text("${snapshot.error}");
        }

        // Por defecto, muestra un loading spinner
        return Container(
            padding: EdgeInsets.all(10),
            child: Center(
                child: CircularProgressIndicator(
              backgroundColor: Colors.indigo[600],
            )));
      },
    );
  }
}
