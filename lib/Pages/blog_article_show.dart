import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class ScreenArguments {
  final String url;
  final String id;
  final String content;
  final String title;

  ScreenArguments({this.url, this.id, this.content, this.title});

  String get getUrl => url;
  String get getContent => content;
  String get getId => id;
  String get getTitle => title;
}

class BlogArticle extends StatefulWidget {
  BlogArticle({Key key}) : super(key: key);

  @override
  _BlogArticleState createState() => _BlogArticleState();
}

class _BlogArticleState extends State<BlogArticle> {
  @override
  Widget build(BuildContext context) {
    final ScreenArguments args = ModalRoute.of(context).settings.arguments;

    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          backgroundColor: Colors.yellow[700],
          title: Text('Post: ' + args.getId),
        ),
        body: Container(
          color: Colors.indigo[50],
          child: ListView(
            children: <Widget>[
              Container(
                color: Colors.deepOrange[300],
                padding: EdgeInsets.all(8.0),
                child: Text(
                  args.getTitle,
                  style: GoogleFonts.mouseMemoirs(
                      fontSize: 20, fontWeight: FontWeight.bold),
                  textAlign: TextAlign.center,
                ),
              ),
              Container(
                color: Colors.white,
                padding: EdgeInsets.all(10.0),
                margin: EdgeInsets.all(8.0),
                child: Text(args.getContent,
                    style: GoogleFonts.workSans(fontSize: 14, height: 1.5),
                    textAlign: TextAlign.justify),
              )
            ],
          ),
        ));
  }
}
