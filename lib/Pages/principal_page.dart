import 'package:expandetetv/Pages/blog_page.dart';
import 'package:expandetetv/Pages/inicio_page.dart';
import 'package:expandetetv/Pages/staff_page.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class PrincipalPage extends StatefulWidget {
  PrincipalPage({Key key}) : super(key: key);

  @override
  _PrincipalPageState createState() => _PrincipalPageState();
}

class _PrincipalPageState extends State<PrincipalPage> {
  List<Widget> _displayOptions = <Widget>[
    InicioPage(),
    BlogPage(),
    StaffPage()
  ];
  int _selectedIndex = 0;
  String _tituloSection = 'ExPandeteTv';
  Color _colorAppbar = Colors.teal[400];

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(_tituloSection),
        backgroundColor: _colorAppbar,
        actions: <Widget>[
          IconButton(icon: Icon(Icons.info_outline), onPressed: (){})
        ],
      ),
      key: widget.key,
      body: _displayOptions.elementAt(_selectedIndex),
      bottomNavigationBar: barraNavigator(),
    );
  }

  BottomNavigationBar barraNavigator() {
    return BottomNavigationBar(
      items: const <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          icon: Icon(Icons.home),
          title: Text('Inicio'),
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.dashboard),
          title: Text('Blog'),
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.supervised_user_circle),
          title: Text('Staff'),
        ),
      ],
      currentIndex: _selectedIndex,
      selectedItemColor: Colors.orange[700],
      backgroundColor: Colors.indigo[50],
      onTap: onTap,
      elevation: 0,
      unselectedLabelStyle: GoogleFonts.ralewayDots(
        fontSize: 14,
        fontWeight: FontWeight.w700,
        fontStyle: FontStyle.normal,
      ),
      selectedLabelStyle: GoogleFonts.ralewayDots(
        fontSize: 14,
        fontWeight: FontWeight.w800,
        fontStyle: FontStyle.normal,
      ),
    );
  }

  void onTap(int value) {
    setState(() {
      _selectedIndex = value;
      if (this._selectedIndex == 0) {
        _tituloSection = 'ExPadeteTV';
        _colorAppbar = Colors.teal[400];
      } else if (_selectedIndex == 1) {
        _tituloSection = 'Blog';
        _colorAppbar = Colors.blue[500];
      } else if (_selectedIndex == 2) {
        _tituloSection = 'Staff';
        _colorAppbar = Colors.yellow[800];
      }
    });
  }
}