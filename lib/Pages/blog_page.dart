import 'dart:convert';
import 'package:expandetetv/Pages/blog_article_show.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';

import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';
import 'package:google_fonts/google_fonts.dart';

class BlogPage extends StatefulWidget {
  BlogPage({Key key}) : super(key: key);

  @override
  _BlogPageState createState() => _BlogPageState();
}

class _BlogPageState extends State<BlogPage> {
  int _cantidadPost = 0;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.blue[800],
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[primerContenedor(context), textoFutureBuilder()],
        ),
      ),
    );
  }

  ClipPath primerContenedor(BuildContext context) {
    return ClipPath(
      clipper: OvalBottomBorderClipper(),
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height * 0.30,
        decoration: BoxDecoration(
          color: Colors.blue,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[logoSection(context), estadisticasBlog()],
        ),
      ),
    );
  }

  Expanded estadisticasBlog() {
    return Expanded(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
              child: Text('Cantidad de Posts:',
                  style: GoogleFonts.mali(
                    color: Colors.white,
                  ))),
          Text('$_cantidadPost',
              style: GoogleFonts.mali(
                color: Colors.black38,
                fontSize: 40,
              )),
        ],
      ),
    );
  }

  Expanded logoSection(BuildContext context) {
    return Expanded(
      child: Image.asset(
        'assets/blog.png',
        height: MediaQuery.of(context).size.height * 0.23,
      ),
    );
  }

  FutureBuilder<ListaArticulosBlog> textoFutureBuilder() {
    return FutureBuilder<ListaArticulosBlog>(
      future: getData(),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return Expanded(
            child: ClipPath(
              clipper: WaveClipperOne(reverse: true),
              child: Container(
                //margin: EdgeInsets.only(top: 30),
                color: Colors.indigo[50],
                child: ListView.builder(
                    shrinkWrap: true,
                    key: widget.key,
                    itemCount: snapshot.data.listaJson.length,
                    itemBuilder: (context, index) {
                      return listTilePosts(snapshot, index);
                    }),
              ),
            ),
          );
        } else if (snapshot.hasError) {
          return Text("${snapshot.error}");
        }

        // Por defecto, muestra un loading spinner
        return Container(
            padding: EdgeInsets.all(10),
            child: Center(
                child: CircularProgressIndicator(
              backgroundColor: Colors.indigo[600],
            )));
      },
    );
  }

  Container listTilePosts(
      AsyncSnapshot<ListaArticulosBlog> snapshot, int index) {
    return Container(
      decoration: BoxDecoration(color: Colors.white),
      margin: EdgeInsets.all(6),
      child: Column(
        children: <Widget>[
          ListTile(
            onTap: () {
              //html.window.open(snapshot.data.listaJson[index].link, 'Articulo');
              Navigator.pushNamed(context, 'articulo',
                  arguments: ScreenArguments(
                      url: snapshot.data.listaJson[index].link,
                      id: snapshot.data.listaJson[index].id,
                      content: removerHTMLTags(
                          snapshot.data.listaJson[index].contentFull),
                      title: snapshot.data.listaJson[index].title));
            },
            trailing: Icon(Icons.arrow_forward_ios, color: Colors.blue[300],),
            title: Text(
              '${snapshot.data.listaJson[index].title}',
              style: TextStyle(height: 1.4),
            ),
            subtitle: Text(
              '${removerHTMLTags(snapshot.data.listaJson[index].content).substring(0, 40)}..',
              style: TextStyle(height: 1.4),
            ),
          ),
        ],
      ),
    );
  }

  String removerHTMLTags(String htmlText) {
    RegExp exp = RegExp(r"<[^>]*>", multiLine: true, caseSensitive: true);

    return htmlText.replaceAll(exp, '');
  }

  Future<ListaArticulosBlog> getData() async {
    final response =
        await http.get('http://systemkw.com/expandetetv/wp-json/wp/v2/posts');
    //print(response.body.toString());
    if (response.statusCode == 200) {
      // Si el servidor devuelve una repuesta OK, parseamos el JSON
      return ListaArticulosBlog.fromJson(json.decode(response.body));
    } else {
      // Si esta respuesta no fue OK, lanza un error.
      throw Exception('Failed to load post');
    }
  }
}

class BlogJson {
  final String id;
  final String title;
  final String content;
  final String link;
  final String contentFull;

  BlogJson({this.id, this.title, this.content, this.link, this.contentFull});

  factory BlogJson.fromJson(Map<String, dynamic> json) {
    return BlogJson(
      id: json['id'].toString(),
      title: json['title']['rendered'],
      content: json['excerpt']['rendered'],
      link: json['guid']['rendered'],
      contentFull: json['content']['rendered'],
    );
  }
}

class ListaArticulosBlog {
  final List<BlogJson> listaJson;

  ListaArticulosBlog({this.listaJson});

  factory ListaArticulosBlog.fromJson(List<dynamic> parsedJson) {
    List<BlogJson> listaJson = new List<BlogJson>();
    listaJson = parsedJson.map((i) => BlogJson.fromJson(i)).toList();

    return new ListaArticulosBlog(
      listaJson: listaJson,
    );
  }
}
